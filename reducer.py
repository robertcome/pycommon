#!/usr/ali/bin/python
#coding=utf-8

import sys

class Reducer(object):
    def __init__(self):
        self._inner_dict = dict()

    def add(self, key, vlist):
        if not self._inner_dict.has_key(key):
            self._inner_dict[key] = [0 for i in range(len(vlist))]

        vref = self._inner_dict[key]
        for i in range(len(vref)):
            vref[i] += int(vlist[i])

    def dump(self):
        for kk, vv in self._inner_dict.items():
            print kk, vv

    def get_dict_ref(self):
        return self._inner_dict

    def __del__(self):
        self._inner_dict.clear()

if __name__ == '__main__':

    rr = Reducer()

    rr.add("k1", [0,1,3,4])
    rr.add("k2", [2,5,4,5])
    rr.add("k1", [2,6,4,2])
    #rr.add("k1", [2,6,4])
    rr.dump()

    dd = rr.get_dict_ref()
    for kk, vv in dd.items():
        print kk, "xx", vv

