#!/usr/ali/bin/python
#coding=utf-8

from optparse import OptionParser 

if __name__ == '__main__':

    usage = "usage: %prog [options] arg1 arg2"
    parser = OptionParser(usage=usage) 

    parser.add_option("-v", "--verbose", action="store_true", \
            dest="verbose", default=False, help="verbose output")
    parser.add_option("-q", "--quiet", action="store_false", \
            dest="quiet", default=True, help="not verbose output")

    parser.add_option("-t", "--type", \
            dest="type", type="int", help="switch type")

    parser.add_option("-f", "--file", \
            dest="filename", help="output filename", metavar="FILE")


    (options, args) = parser.parse_args() 

    if options.verbose:
        print "verbose"

    if options.quiet:
        print "quiet"

    print options.filename

    print options.type

    if len(args) < 1:
        parser.print_help()
    else:
        print args[0]
